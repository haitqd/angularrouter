import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetailEmployeeComponent } from './detail-employee/detail-employee.component';
import { ListEmployeeComponent } from './list-employee/list-employee.component';
import { PageErrorComponent } from './page-error/page-error.component';

const routes: Routes = [] = [
  { path: '', redirectTo: '/detailEmployee', pathMatch: 'full' },
  { path: 'detailEmployee', component: DetailEmployeeComponent },
  { path: 'listEmployee', component: ListEmployeeComponent },
  { path: '**', component: PageErrorComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
